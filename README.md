# Primer on Python Decorators

Python Programming에서 많이 사용되는 decorator에 대하여 간단히 알아보도록 한다. 본 페이지는 Geir Arne Hjelle가 작성한 [Primer on Python Decorators](https://realpython.com/primer-on-python-decorators/)저자의 동의없이 개인적인 관심에서 편역한 것이다.

좋은 Python 프로그래머가 되고 싶은 사람들에게 조금이라도 도움이 되길 바란다.

created on Dec. 3, 2022

