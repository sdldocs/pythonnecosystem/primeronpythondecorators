## <a name='functions'>Functions</a>
decorator를 이해하기 전에 먼저 함수가 어떻게 작동하는지 이해해야 한다. 우리의 목적을 위해 함수는 주어진 인수를 기반으로 값을 반환한다. 다음은 매우 간단한 예이다.

```
>>> def add_one(number):
...     return number + 1

>>> add_one(2)
3
```

일반적으로 Python에서 함수는 입력을 출력으로 바꾸는 것보다 부작용(side effects)이 있을 수 있다. [print() 함수](https://realpython.com/python-print/)가 이에 대한 기본적인 예이다. 이 함수는 [`None `을 반환하며](https://realpython.com/python-return-statement/) 콘솔에 무언가를 출력하는 부작용이 있다. 그러나 decorator를 이해하기 위해서는 주어진 인수를 값으로 변환하는 것으로 함수를 생각하는 것으로 충분하다.

> **Note:** [함수형 프로그래밍(functional programming)](https://realpython.com/python-functional-programming/)에서는 (거의) 부작용 없이 순수한 함수로만 동작한다. Python은 순수하게 함수형 언어는 아니지만, 함수를 first-class 객체에 포함시킨 많은 함수형 프로그래밍 개념을 지원한다.

### <a name='first-class-objects'>First-Class 객체</a>
Python에서 함수는 [first-class 객체이](https://dbader.org/blog/python-first-class-functions)다. 이는 [문자열, int, float, list 등과 같은 객체](https://realpython.com/primer-on-python-decorators/#:~:text=any%20other%20object%20(string%2C%20int%2C%20float%2C%20list%2C%20and%20so%20on))와 마찬가지로 **함수를 인수로 전달하고 사용될 수 있다**는 것을 의미한다. 다음 세 가지 함수을 살펴보자.

```
def say_hello(name):
    return f"Hello {name}"

def be_awesome(name):
    return f"Yo {name}, together we are the awesomest!"

def greet_bob(greeter_func):
    return greeter_func("Bob")
```

여기서 `say_hello()`와 `be_awesome()`은 문자열로 주어진 이름을 인수로 기대하는 일반 함수이다. 그러나 `greet_bob()` 함수는 함수를 인수로 사용한다. 예를 들어, `say_hello()` 또는 `be_awesome()` 함수를 인수로 전달할 수 있다.

```
>>> greet_bob(say_hello)
'Hello Bob'

>>> greet_bob(be_awesome)
'Yo Bob, together we are the awesomest!'
```

`greet_bob(say_hello)`은 `greet_bob()`과 `say_hello` 두 함수를 참조(refer)한다. `say_hello` 함수는 괄호 없이 이름만 지정된다. 즉, 함수에 대한 참조만 전달한다. 함수가 실행되지 않았다. 반면 `greet_bob()` 함수는 괄호가 있으므로 평소와 같이 호출된다.

### <a name='inner-functions'>내부 함수</a>
*함수 내부*에서 [다른 함수를 정의](https://realpython.com/defining-your-own-python-function/)할 수 있다. 이 함수를 [내부 함수](https://realpython.com/inner-functions-what-are-they-good-for/)라고 한다. 다음은 내부 함수가 둘인 함수의 예이다.

```
def parent():
    print("Printing from the parent() function")

    def first_child():
        print("Printing from the first_child() function")

    def second_child():
        print("Printing from the second_child() function")

    second_child()
    first_child()
```

`parent()` 함수를 호출하면 어떻게 될까? 이것에 대해 잠시 생각해 보자. 출력은 다음과 같다.

```
>>> parent()
Printing from the parent() function
Printing from the second_child() function
Printing from the first_child() function
```

내부 함수가 정의되는 순서는 중요하지 않다. 다른 함수와 마찬가지로 내부 함수를 실행할 때만 인쇄를 수행한다.

더욱이, 내부 함수는 `parent` 함수가 호출될 때까지 정의되지 않는다. 이들은 `parent()`를 기준으로 로컬 범위(local scope)에서 지정되며, `parent()` 함수 내부에서만 로컬 [변수](https://realpython.com/python-variables/)로만 존재한다. first_child()를 호출해 보자. 다음과 같운 오류가 나타날 것이다.

```
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'first_child' is not defined
```

`parent()`를 호출할 때마다 내부 함수 `first_child()`와 `second_child()`도 호출된다. 그러나 로컬 범위 때문에 `parent()` 함수 외부에서는 사용할 수 없다.

### <a name='returning-functions-from-functions'>함수에서 함수 반환</a>
Python에서는 함수를 반환 값으로 사용할 수도 있다. 다음 예에서 외부 `parent()` 함수에서 내부 함수 중 하나를 반환한다.

```
def parent(num):
    def first_child():
        return "Hi, I am Emma"

    def second_child():
        return "Call me Liam"

    if num == 1:
        return first_child
    else:
        return second_child
```

소괄호('()') 없이 `first_child`를 반환한다. 이는 `first_child` 함수에 대한 참조를 반환한다는 것을 의미한다. 반대로 소괄호가 있는 `first_child()`는 함수를 평가한 결과를 나타낸다. 이는 다음 예에서 확인할 수 있다.

```
>>> first = parent(1)
>>> second = parent(2)

>>> first
<function parent.<locals>.first_child at 0x7f599f1e2e18>

>>> second
<function parent.<locals>.second_child at 0x7f599dad5268>
```

다소 암호같은 출력은 단순히 `first` 변수가 `parent()` 내부의 로컬 `first_child()` 함수를 참조하는 반면 `second` 변수는 `second_child()`를 가리킨다는 것을 의미한다.

비록 함수가 가리키는 함수는 직접 액세스할 수 없지만, 이제 `first`와 `second`를 일반 함수처럼 사용할 수 있다. 

```
>>> first()
'Hi, I am Emma'

>>> second()
'Call me Liam'
```

마지막으로, 이전 예제에서는 `parent 함수 내에서 내부 함수 first_child()`를 실행했다. 그러나 마지막 예에서는 반환할 때 first_child라는 내부 함수에 괄호를 추가하지 않았다. 이 방법으로, 여러분은 미래에 여러분이 호출할 수 있는 각 함수에 대한 참조를 얻었다. 이해했나요?

## <a name='simple-decorators'>간단한 Decorators</a>
이제 함수가 Python의 다른 객체와 동일하다는 것을 알았으므로 다음 단계로 나아가 Python decorator의 마법을 볼 준비가 되었다. 예를 들어 보자.

```
def my_decorator(func):
    def wrapper():
        print("Something is happening before the function is called.")
        func()
        print("Something is happening after the function is called.")
    return wrapper

def say_whee():
    print("Whee!")

say_whee = my_decorator(say_whee)
```

`say_whee()`를 호출하면 어떻게 되는지 추측할 수 있을까요? 시도해 봅시다.

```
>>> say_whee()
Something is happening before the function is called.
Whee!
Something is happening after the function is called.
```

여기서 무슨 일이 일어나고 있는지 이해하기 위해, 이전의 예들을 다시 살펴보자. 우리는 말 그대로 여러분이 지금까지 배운 모든 것을 적용하고 있는 것이다.

이른바 decoration은 다음 줄에서 발생한다.

```
say_whee = my_decorator(say_whee)
```

실제로 `say_whee`라는 이름은 이제 `wrapper()` 내부 함수를 가리킨다. `my_decorator(say_whee)`를 호출할 때 함수로 `wrapper`를 반환한다는 것을 기억하자.

```
>>> say_whee
<function my_decorator.<locals>.wrapper at 0x7f3c5dfd42f0>
```
그러나 `wrapper()`는 원래의 `say_whee()`를 `func`으로 참조하며, 두 함수 `print()` 호출 사이레서 `say_whee()`를 호출한다.

간단히 말해서, **decorator는 함수을 포장하고, 함수의 작업을 변경한다.**

다음으로 넘어가기 전에 두 번째 예를 살펴보자. `wrapper()`가 일반 Python 함수이기 때문에 decorator가 함수를 수정하는 방식은 동적으로 변할 수 있다. 이웃을 방해하지 않도록 다음 예제에서는 낮에만 decorate된 코드를 실행한다.

```python
from datetime import datetime

def not_during_the_night(func):
    def wrapper():
        if 7 <= datetime.now().hour < 22:
            func()
        else:
            pass  # Hush, the neighbors are asleep
    return wrapper

def say_whee():
    print("Whee!")

say_whee = not_during_the_night(say_whee)
```

취침 후 `say_whee()`를 호출하면 아무 일도 일어나지 않는다.

```
>>> say_whee()
>>>
```

### <a name='syntactic-sugar'>Syntactic Sugar!</a>
위의 `say_whee()`를 decorate한 방식은 조금 투박하다. 우선, 여러분은 `say_whee`라는 이름을 세 번 타이핑했다. 게다가, decoration은 함수의 정의 아래에 약간 숨겨져 있다.

대신 Python을 사용하면 (종종 ["pie" 구문](https://www.python.org/dev/peps/pep-0318/#background)이라고도 하는) **@ 기호로 decrator를 더 간단하게 사용**할 수 있다. 다음 예제는 첫 번째 decorator 예제와 같은 작업을 수행한다.

```python
def my_decorator(func):
    def wrapper():
        print("Something is happening before the function is called.")
        func()
        print("Something is happening after the function is called.")
    return wrapper

@my_decorator
def say_whee():
    print("Whee!")
```

그래서, `@my_recator`는 `say_whee = my_recator(say_whee)`의 더 쉬운 방법일 뿐이다. 이것이 함수에 decorator를 적용하는 방식이다.

### <a name='reusing-decorators'>Decorators 재활용</a>
decorator는 단지 일반적인 Python 함수이라는 것을 기억하자. 쉽게 재사용할 수 있도록 모든 일반적인 도구로 사용할 수 있다. decorator를 다른 많은 함수에서 사용할 수 있는 자체 [모듈](https://realpython.com/python-modules-packages/)로 이동한다.

다음 내용을 포함하는 `decorators.py`라는 파일을 만든다.

```python
def do_twice(func):
    def wrapper_do_twice():
        func()
        func()
    return wrapper_do_twice
```

> **Note:** 내부 함수의 이름을 원하는 대로 지정할 수 있으며, `wrapper()`와 같은 일반적인 이름도 대개는 괜찮다. 여러분은 이 튜토리얼에서 많은 decorator를 볼 수 있을 것이다. 이들을 구분하기 위해 내부 함수의 이름은 decorator와 동일하지만 `wrapper_`를 붙여 시작한다.

이제 `import`를 수행하여 다른 파일에서 이 새로운 decorator를 사용할 수 있다.

```python
from decorators import do_twice

@do_twice
def say_whee():
    print("Whee!")
```

이 예제를 실행하면 원래 say_whee()가 두 번 실행되는 것을 볼 수 있다.

```
>>> say_whee()
Whee!
Whee!
```

### <a name='decorating-with-arguments'>인수가 있는 Decorating 함수</a>
인수가 있는 함수가 있다고 하자. decorate할 수 있나요? 해 보자.

```python
from decorators import do_twice

@do_twice
def greet(name):
    print(f"Hello {name}")
```

불행히도 이 코드를 실행하면 다음과 같은 오류가 발생한다.

```
>>> greet("World")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: wrapper_do_twice() takes 0 positional arguments but 1 was given
```

문제는 내부 함수 `wrapper_do_twice()`에는 인수가 정의되어 있지 않았는데 파라미터 `name="World"`가 전달되었다. `wrapper_do_twice()`가 하나의 인수를 허용함으로써 이 문제를 해결할 수 있지만, 이전에 생성한 `say_whee()` 함수에서는 작동하지 않는다.

해결책은 [*args와 **kwrg](https://realpython.com/python-kwargs-and-args/)를 내부 `wrapper` 함수에 사용하는 것이다. 그러면 임의 개수의 positional 및 keyword 인수를 허용한다. `decorators.py`를 다음과 같이 다시 작성한다.

```python
def do_twice(func):
    def wrapper_do_twice(*args, **kwargs):
        func(*args, **kwargs)
        func(*args, **kwargs)
    return wrapper_do_twice
```

`wrapper_do_twice()` 내부 함수는 이제 임의의 수의 인수를 허용하고 이 인수를 그것이 decorate하는 함수로 전달한다. 이제 `say_whee()`와 `greet()` 예 모두 작동한다.

```
>>> say_whee()
Whee!
Whee!

>>> greet("World")
Hello World
Hello World
```

### <a name='returning-values'>Decorated 함수의 반환 값</a>
decorate된 함수의 반환 값은 무엇일까? 글쎄, 그건 decorator가 결정한다. 간단한 함수를 다음과 같이 정의하였다고 가정하자.

```python
from decorators import do_twice

@do_twice
def return_greeting(name):
    print("Creating greeting")
    return f"Hi {name}"
```

그것을 해 보자.

```
>>> hi_adam = return_greeting("Adam")
Creating greeting
Creating greeting
>>> print(hi_adam)
None
```

decorator가 함수의 반환 값을 계산했다.

`do_twice_wrapper()`가 명시적으로 값을 반환하지 않기 때문에 `return_greeting("Adam")` 호출은 `None`을 반환한다.

이를 수정하려면 **wrapper 함수가 decorate된 함수의 반환 값을 반환하는지 확인**해야 한다. `decorators.py` 파일을 수정한다.

```python
def do_twice(func):
    def wrapper_do_twice(*args, **kwargs):
        func(*args, **kwargs)
        return func(*args, **kwargs)
    return wrapper_do_twice
```

함수의 최종 실행으로부터의 반환 값은 다음과 같다.

```python
>>> return_greeting("Adam")
Creating greeting
Creating greeting
'Hi Adam'
```

### <a name='who-are-you'>Who Are You, Really?</a>
특히 대화형 쉘에서 Python으로 프로그래밍할 때 가장 편리한 것은 강력한 자기 성찰(Introspection) 능력이다. [자기 성찰](https://en.wikipedia.org/wiki/Type_introspection)은 런타임에 객체가 자신의 속성을 알 수 있는 능력이다. 예를 들어, 함수는 자신의 이름과 [문서](https://realpython.com/documenting-python-code/)를 알고 있다:

```
>>> print
<built-in function print>

>>> print.__name__
'print'

>>> help(print)
Help on built-in function print in module builtins:

print(...)
    <full help message>
```

자기 성찰은 여러분이 스스로를 정의하는 함수에 대해 동작한다.

```
>>> say_whee
<function do_twice.<locals>.wrapper_do_twice at 0x7f43700e52f0>

>>> say_whee.__name__
'wrapper_do_twice'

>>> help(say_whee)
Help on function wrapper_do_twice in module decorators:

wrapper_do_twice()
```

그러나 decorate된 다음 `say_whee()`의 정체성에 대해 매우 혼란스러워졌다. 이제 `do_twice()` decorator 내부의 `wrapper_do_twice()` 내부 함수라고 반환한다. 비록 기술적으로는 사실이지만, 이는 매우 유용하지 않다.

이 문제를 해결하기 위해, decorator는 [@funtools.wraps](https://docs.python.org/library/functools.html#functools.wraps) decorator를 사용해야 하며, 원래 함수에 대한 정보를 보존할 것이다. `decorators.py`를 다시 업데이트한다.

```python
import functools

def do_twice(func):
    @functools.wraps(func)
    def wrapper_do_twice(*args, **kwargs):
        func(*args, **kwargs)
        return func(*args, **kwargs)
    return wrapper_do_twice
```

decorate된 `say_whee()` 함수에 대해 아무것도 변경할 필요가 없다.

```
>>> say_whee
<function say_whee at 0x7ff79a60f2f0>

>>> say_whee.__name__
'say_whee'

>>> help(say_whee)
Help on function say_whee in module whee:

say_whee()
```

훨씬 나아졌네요! 이제 `say_whee()`는 decoration 후에도 여전히 그 자체라고 말하고 있다.

> **Note**: `@funtools.wraps` decorator는 `functools.update_wrapper()` 함수를 [사용](https://github.com/python/cpython/blob/5d4cb54800966947db2e86f65fb109c5067076be/Lib/functools.py#L34)하여 내부 성찰에 사용되는 `__name__` 및 `__doc__`과 같은 특수 어트리뷰트를 업데이트한다.

## <a name='real-world-examples'>실전 예</a>
decorator의 유용한 몇몇 예를 더 살펴보자. 여러분은 그들이 주로 여러분이 지금까지 배운 것과 같은 패턴을 따른다는 것을 알게 될 것이다.

```python
import functools

def decorator(func):
    @functools.wraps(func)
    def wrapper_decorator(*args, **kwargs):
        # Do something before
        value = func(*args, **kwargs)
        # Do something after
        return value
    return wrapper_decorator
```

위 코드은 더 복잡한 decorator를 작성 하기에 좋은 상용구 템플릿이다.

> **Note:** 나중의 예에서는 이러한 decorator들이 `decorators.py` 파일에도 저장된다고 가정한다. [이 튜토리얼의 모든 예들](https://github.com/realpython/materials/tree/master/primer-on-python-decorators)을 다운로드할 수 있다.

### <a name='timing-functions'>타이밍 함수</a>
@timer decorator를 만드는 것부터 시작하자. [함수을 실행하고 실행 시간을 측정하고](https://realpython.com/python-timer/) 콘솔에 이를 인쇄한다. 코드는 다음과 같다.

```python
import functools
import time

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer

@timer
def waste_some_time(num_times):
    for _ in range(num_times):
        sum([i**2 for i in range(10000)])
```

이 decorator는 함수가 실행되기 직전(#1 줄)과 함수가 종료된 직후(#2 줄)의 시간을 저장함으로써 동작한다. 함수 실행에 걸리는 시간은 둘 사이의 차이이다(#3 줄). 우리는 [time_perf_counter()](https://docs.python.org/library/time.html#time.perf_counter) 함수을 사용하며, 이는 시간 간격을 잘 측정한다. 다음은 타이밍의 몇 가지 예이다.

```python
>>> waste_some_time(1)
Finished 'waste_some_time' in 0.0010 secs

>>> waste_some_time(999)
Finished 'waste_some_time' in 0.3260 secs
```

직접 실행해 보자. 코드를 한 줄씩 살펴보자. 동작 방식을 이해했는지 확인하시오. 이해 못해도 걱정하지 마시오. decorator들은 고급 사용 기술이다. 잘 생각해보거나 프로그램 흐름을 그려보도록 해보자.

> **Note:** `@timer` decorator는 함수 런타임에 대한 아이디어를 얻고 싶을 때 유용하다. 코드 수행시간를 보다 정확하게 측정하려면 표준 라이브러리의 [timeit 모듈](https://docs.python.org/library/timeit.html)을 사용해야 한다. [가비지 수집](https://realpython.com/python-memory-management/#garbage-collection)을 일시적으로 비활성화하고 여러 번 실행하여 함수 호출 결과 중 가장 빠른 것을 택하는 것으로 노이즈를 제거한다.

### <a name='debugging-code'>디버깅 코드</a>
다음 `@debug` decorator는 함수를 호출할 때마다 반환 값뿐만 아니라 함수가 호출될 때 파라미터 값를 인쇄한다.

```python
import functools

def debug(func):
    """Print the function signature and return value"""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
        signature = ", ".join(args_repr + kwargs_repr)           # 3
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")           # 4
        return value
    return wrapper_debug
```

시그니처는 모든 인수의 [문자열 표현](https://dbader.org/blog/python-repr-vs-str)을 결합하여 생성된다. 다음 목록의 숫자는 코드의 주석에 있는 줄 번호에 대한 설명이다.

1. positional 인수 목록을 만든다. `repr()`을 사용하여 각 인수를 나타내는 문자열을 얻는다.
2. 키워드 인수 목록을 만든다. [f-string](https://realpython.com/python-f-strings/)은 각 인수를 `key=value`로 포맷한다. 여기서 `!r` 지정자는 `repr()` 값을 나타내는 데 사용됨을 의미한다.
3. positional 및 keyword 인수 목록은 쉼표로 구분된 각 인수와 함께 한 시그니처 문자열에 결합된다.
4. 반환 값은 기능이 실행된 후에 인쇄된다.
5. positional 인수와 keyword 인수가 각각 하나인 간단한 함수에 적용하여 decorator가 실제로 어떻게 작동하는지 알아보자.

```python
@debug
def make_greeting(name, age=None):
    if age is None:
        return f"Howdy {name}!"
    else:
        return f"Whoa {name}! {age} already, you are growing up!"
```

`@debug` decorator가 `make_greeting()` 함수의 시그니처와 반환 값을 인쇄하는 방법을 주목하시오.

```
>>> make_greeting("Benjamin")
Calling make_greeting('Benjamin')
'make_greeting' returned 'Howdy Benjamin!'
'Howdy Benjamin!'

>>> make_greeting("Richard", age=112)
Calling make_greeting('Richard', age=112)
'make_greeting' returned 'Whoa Richard! 112 already, you are growing up!'
'Whoa Richard! 112 already, you are growing up!'

>>> make_greeting(name="Dorrisile", age=116)
Calling make_greeting(name='Dorrisile', age=116)
'make_greeting' returned 'Whoa Dorrisile! 116 already, you are growing up!'
'Whoa Dorrisile! 116 already, you are growing up!'
```

이 예는 `@debug` decorator가 방금 작성한 내용을 반복하기 때문에 즉시 유용하지 않을 수 있다. 직접 부르지 않는 작은 편의 함수를 적용하면 더욱 강력해진다.

다음 예제에서는 수학 상수 e에 대한 근사치를 계산한다.

```python
import math
from decorators import debug

# Apply a decorator to a standard library function
math.factorial = debug(math.factorial)

def approximate_e(terms=18):
    return sum(1 / math.factorial(n) for n in range(terms))
```

이 예제에서는 이미 정의된 함수에 decorator를 적용하는 방법을 보인다. 수학 상수 `e`의 근사치는 다음과 같은 [급수 전개](https://en.wikipedia.org/wiki/E_(mathematical_constant))에 기초한다.

`approximate_e()` 함수를 호출하면 `@debug` decorator 동작을 볼 수 있다.

```python
>>> approximate_e(5)
Calling factorial(0)
'factorial' returned 1
Calling factorial(1)
'factorial' returned 1
Calling factorial(2)
'factorial' returned 2
Calling factorial(3)
'factorial' returned 6
Calling factorial(4)
'factorial' returned 24
2.708333333333333
```

이 예제에서는 5개 항만 더하면 실제 값 `e = 2.7182828`에 대한 적절한 근사치를 구할 수 있다.

### <a name='slowing-down-code'>감속 코드</a>
이 다음 예는 그다지 유용하지 않은 것처럼 보일 수 있다. 왜 python 코드의 수행을 늦추려고 할까? 가장 일반적인 사용 사례는 웹 페이지와 같은 리소스가 변경되었는지 여부를 지속적으로 확인하는 함수의 수행을 제한하려는 경우이다. `@slow_decorator`는 decorated 함수를 호출하기 전에 1초간 절전 모드로 전환한다.

```python
import functools
import time

def slow_down(func):
    """Sleep 1 second before calling the function"""
    @functools.wraps(func)
    def wrapper_slow_down(*args, **kwargs):
        time.sleep(1)
        return func(*args, **kwargs)
    return wrapper_slow_down

@slow_down
def countdown(from_number):
    if from_number < 1:
        print("Liftoff!")
    else:
        print(from_number)
        countdown(from_number - 1)
```

`@slow_down` decorator의 효과를 보기 위한 실제로 예를 실행해 본다.

```
>>> countdown(3)
3
2
1
Liftoff!
```

> **Note:** `countdown()` 함수는 재귀 함수이다. 즉, 자신을 호출하는 함수이다. Python의 재귀 함수에 대한 자세한 내용은 [Python의 재귀적 사고](https://realpython.com/python-thinking-recursively/)에 대한 가이드를 참조하시오.

`@slow_down` decorator는 항상 1초 동안 절전 모드에 있는다. 나중에 decorator에게 인수를 전달하여 길이를 제어하는 방법을 알아보자.

### <a name='registering-plugin'>플러그인 등록</a>
decorator들은 그들의 decorate 함수을 decoration할 필요가 없다. 그들은 또한 단순히 함수가 존재한다는 것을 등록하고 그것을 포장하지 않고 반환할 수 있다. 예를 들어 경량 플러그인 아키텍처를 만드는 데 사용할 수 있다.

```python
import random
PLUGINS = dict()

def register(func):
    """Register a function as a plug-in"""
    PLUGINS[func.__name__] = func
    return func

@register
def say_hello(name):
    return f"Hello {name}"

@register
def be_awesome(name):
    return f"Yo {name}, together we are the awesomest!"

def randomly_greet(name):
    greeter, greeter_func = random.choice(list(PLUGINS.items()))
    print(f"Using {greeter!r}")
    return greeter_func(name)
```

`@register` decorator는 단순히 글로벌 PLUGINS dict에 decorate된 함수에 대한 참조를 저장한다. 수정되지 않은 원래 함수를 반환하기 때문에 내부 함수를 작성하거나 `@functools.wraps`를 사용할 필요가 없다.

`randomly_greet()` 함수는 사용할 등록된 함수 중 하나를 임의로 선택한다. PLUGINS dict에는 플러그인으로 등록된 각 함수 객체에 대한 참조가 이미 포함되어 있다.

```python
>>> PLUGINS
{'say_hello': <function say_hello at 0x7f768eae6730>,
 'be_awesome': <function be_awesome at 0x7f768eae67b8>}

>>> randomly_greet("Alice")
Using 'say_hello'
'Hello Alice'
```

`@register` decorator를 사용하여 `globals()`에서 일부 함수를 효과적으로 직접 선택하여 관심 변수의 큐레이션된 목록을 만들 수 있다.

### <a name='user-logged-in'>로그인한 사용자?</a>
일부 화려한 decorator들로 넘어가기 전의 마지막 예는 웹 프레임워크로 작업할 때 일반적으로 사용된다. 이 예에서는 [Flask](https://realpython.com/tutorials/flask/)를 사용하여 로그인하여 인증된 사용자만 볼 수 있는 `/secre`t 웹 페이지를 설정한다.

```python
from flask import Flask, g, request, redirect, url_for
import functools
app = Flask(__name__)

def login_required(func):
    """Make sure user is logged in before proceeding"""
    @functools.wraps(func)
    def wrapper_login_required(*args, **kwargs):
        if g.user is None:
            return redirect(url_for("login", next=request.url))
        return func(*args, **kwargs)
    return wrapper_login_required

@app.route("/secret")
@login_required
def secret():
    ...
```

이는 웹 프레임워크에서 인증을 추가하는 방법에 대한 아이디어를 제공하지만, 일반적으로 이러한 유형의 decorator를 직접 작성하지 않는다. Flask의 경우 보안 및 기능을 추가하는 [Flask-Login 확장](https://flask-login.readthedocs.io/en/latest/#flask_login.login_required)을 대신 사용할 수 있다.

