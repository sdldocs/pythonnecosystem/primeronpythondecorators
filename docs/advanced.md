## <a name='fancy-decorators'>멋진 decorator</a>
지금까지 간단한 decorator을 만드는 방법을 살펴보았다. 여러분은 이미 decorator가 무엇이고 그것들이 어떻게 작동하는지에 대해 꽤 잘 알고 있을 것이다. 이 튜토리얼을 잠시 쉬면서 배운 모든 것을 연습해 보세요.

이 튜토리얼의 두 번째 부분에서는 다음 사용 방법을 포함한 고급 기능에 대해 알아보겠다.

- [틀래스 decorating](#decorating-classes)
- [중첩 decorator](#nesting-decorators)
- [인수가 있는 decorator](#decorators-with-arguments)
- [둘 다 주세요, 하지만 빵은 됐어요](#decorators-optional-arguments)
- [상태를 저장하는 decorators](#stateful-decorators)
- [decorator로 클래스](#classes-as-decorators)

### <a name='decorating-classes'>클래스 decorating</a>
클래스에서 decorator를 사용할 수 있는 두 가지 방법이 있다. 첫 번째는 여러분이 이미 함수에서 한 것과 매우 유사하여 여러분은 **클래스의 메서드를 decorate할** 수 있다. 이것이 예전에 decorator를 소개하게 된 [동기 중 하나](https://www.python.org/dev/peps/pep-0318/#motivation)였다.

Python에 내장되어 있는 decorator 중에는 [@class method, @static method](https://realpython.com/instance-class-and-static-methods-demystified/), [@property](https://realpython.com/python-property/) 등이 있다. `@classmethod` 및 `@staticmethod` decorator는 클래스의 특정 인스턴스에 연결되어 있지 않고 클래스 [네임스페이스](https://realpython.com/python-namespaces-scope/) 내의 메서드를 정의하는 데 사용된다. @property decorator는 클래스 속성에 대한 [getters 및 setters](hhttps://realpython.com/python-getter-setter/)를 커스터마이즈하는 데 사용된다. 이러한 decorator를 아래와 같이 사용한다.

Circle 클래스의 다음 정의는 `@classmethod`, `@staticmethod`와 `@property` decorator를 사용한다.

```python
class Circle:
    def __init__(self, radius):
        self._radius = radius

    @property
    def radius(self):
        """Get value of radius"""
        return self._radius

    @radius.setter
    def radius(self, value):
        """Set radius, raise error if negative"""
        if value >= 0:
            self._radius = value
        else:
            raise ValueError("Radius must be positive")

    @property
    def area(self):
        """Calculate area inside circle"""
        return self.pi() * self.radius**2

    def cylinder_volume(self, height):
        """Calculate volume of cylinder with circle as base"""
        return self.area * height

    @classmethod
    def unit_circle(cls):
        """Factory method creating a circle with radius 1"""
        return cls(1)

    @staticmethod
    def pi():
        """Value of π, could use math.pi instead though"""
        return 3.1415926535
```

이 클래스에서

- `.cyliner_volume()`은 일반적인 메서드이다.
- `.radius`는 변경 가능한 속성으로, 다른 값으로 설정할 수 있다. 그러나 setter 메서드를 정의하여 오류 테스트를 하여 의미 없는 음수로 설정되지 않았는지 확인할 수 있다. 속성은 괄호 없이 속성으로 접근된다.
- `.area`는 변경할 수 없는 속성이다. `.setter()` 메서드가 없는 속성은 변경할 수 없다. 메서드로 정의하더라도 괄호 없이 속성으로 검색할 수 있다.
- `.unit_circle()`은 클래스 메서드이다. Circle의 특정 인스턴스에 바인딩되지 않는다. 클래스 메소드는 종종 클래스의 특정 인스턴스를 만들 수 있는 공장 메소드로 사용된다.
- `.pi`는 정적 메서드이다. 네임스페이스의 일부인 것을 제외하고는 Circle 클래스에 실제로 종속되지 않는다. 정적 메서드는 인스턴스 또는 클래스에서 호출할 수 있다.

예를 들어 Circle 클래스는 다음과 같이 사용할 수 있다.

```python
>>> c = Circle(5)
>>> c.radius
5

>>> c.area
78.5398163375

>>> c.radius = 2
>>> c.area
12.566370614

>>> c.area = 100
AttributeError: can't set attribute

>>> c.cylinder_volume(height=4)
50.265482456

>>> c.radius = -1
ValueError: Radius must be positive

>>> c = Circle.unit_circle()
>>> c.radius
1

>>> c.pi()
3.1415926535

>>> Circle.pi()
3.1415926535
```

이전의 `@debug` 및 `@timer` decorator를 사용하여 메소드 중 일부를 decorate하는 클래스를 정의한다.

```python
from decorators import debug, timer

class TimeWaster:
    @debug
    def __init__(self, max_num):
        self.max_num = max_num

    @timer
    def waste_time(self, num_times):
        for _ in range(num_times):
            sum([i**2 for i in range(self.max_num)])
```

이 클래스를 사용하여 decorator의 효과를 확인할 수 있다.

```python
>>> tw = TimeWaster(1000)
Calling __init__(<time_waster.TimeWaster object at 0x7efccce03908>, 1000)
'__init__' returned None

>>> tw.waste_time(999)
Finished 'waste_time' in 0.3376 secs
```

클래스에서 decorator를 사용하는 또 다른 방법은 클래스 전체를 decorate하는 것이다. 예를 들어, [Python 3.7](https://realpython.com/python37-new-features/)의 새로운 [데이터 클래스 모듈](https://realpython.com/python-data-classes/)에서 다음과 같이 수행된다.

```python
from dataclasses import dataclass

@dataclass
class PlayingCard:
    rank: str
    suit: str
```

구문의 의미는 함수 decorator와 유사하다. 위의 예제에서는 `PlayingCard = dataclass(PlayingCard)`를 작성하여 decorator을 수행할 수 있다.

[틀래스 decorator의 일반적인 사용](https://www.python.org/dev/peps/pep-3129/#rationale)은 [메타 클래스](https://realpython.com/python-metaclasses/)의 일부 사용 사례에 대한 간단한 대안이 되는 것이다. 두 경우 모두 클래스의 정의를 동적으로 변경한다.

클래스 decorator를 작성는 것은 함수 decorator를 작성는 것과 매우 비슷하다. 유일한 차이점은 decorator가 인수로 함수가 아닌 클래스를 받는다는 것이다. 사실, 위에서 본 모든 decorator들은 클래스 decorator로 작동할 것이다. 함수 대신 클래스에서 사용할 경우 해당 효과가 원하는 것이 아닐 수 있다. 다음 예제에서는 `@timer` decorator를 클래스에 적용한다.

```python
from decorators import timer

@timer
class TimeWaster:
    def __init__(self, max_num):
        self.max_num = max_num

    def waste_time(self, num_times):
        for _ in range(num_times):
            sum([i**2 for i in range(self.max_num)])
```

클래스를 decorate하는 것은 클래스 메서드를 decorate하는 것이 아니다. `@timer`는 `TimeWasters = timer(TimeWasters)의 약어일 뿐이다.

여기서 `@timer`는 클래스를 인스턴스화하는 데 걸리는 시간만 측정한다.

```python
>>> tw = TimeWaster(1000)
Finished 'TimeWaster' in 0.0000 secs

>>> tw.waste_time(999)
>>>
```

[나중에](https://realpython.com/primer-on-python-decorators/#creating-singletons) 클래스의 인스턴스가 하나만 있는지 확인하는 `@singleton`과 같은 적절한 클래스 decorator를 정의하는 예제를 볼 수 있다.

### <a name='nesting-decorators'>중첩 decorator</a>
여러 decorator를 겹쳐 함수에 적용할 수 있다.

```python
from decorators import debug, do_twice

@debug
@do_twice
def greet(name):
    print(f"Hello {name}")
```

이것을 decorator들이 작성된 순서대로 실행되는 것으로 생각해보자. 즉, `@debug`는 `@do_twice`를 호출하고, `greet()` 또는 `debug(do_twice(greet())`를 호출한다.

```python
>>> greet("Eva")
Calling greet('Eva')
Hello Eva
Hello Eva
'greet' returned None
```

`@debug`와 `@do_twice` 순서를 변경할 경우 차이를 관찰해 보자.

```python
from decorators import debug, do_twice

@do_twice
@debug
def greet(name):
    print(f"Hello {name}")
```

이 경우 `@do_twice`는 `@debug`에도 적용된다.

```python
>>> greet("Eva")
Calling greet('Eva')
Hello Eva
'greet' returned None
Calling greet('Eva')
Hello Eva
'greet' returned None
```

### <a name='decorators-with-arguments'>인수가 있는 decorator</a>
때때로, 여러분의 decorator에게 인수을 전달하는 것이 필요할 때가 있다. 예를 들어, `@do_twice`는 `@repeat(num_times)` decorator로 확장될 수 있다. 그런 다음 decorate된 함수를 실행하는 횟수를 인수로 지정할 수 있다.

이렇게 하면 다음과 같은 작업을 수행할 수 있습니다.

```python
@repeat(num_times=4)
def greet(name):
    print(f"Hello {name}")
```

```python
>>> greet("World")
Hello World
Hello World
Hello World
Hello World
```

여러분이 이것을 어떻게 핳 수 있을지 생각해 보세요.

지금까지 `@` 뒤에 쓰인 이름은 다른 함수와 같이 호출할 수 있는 함수 객체를 지칭했다. 일관성을 유지하려면 `repeat(num_times=4)`을 사용하여 decorator 역할을 할 수 있는 함수 객체를 반환해야 한다. 다행히도, 여러분은 이미 [함수를 반환하는 방법](https://realpython.com/primer-on-python-decorators/#returning-functions-from-functions)을 알고 있다! 일반적으로 다음과 같은 것이 필요하다.

```python
def repeat(num_times):
    def decorator_repeat(func):
        ...  # Create and return a wrapper function
    return decorator_repeat
```

일반적으로 decorator는 내부 wrapper 함수를 생성하고 반환하므로 예제를 전체적으로 작성하면 내부 함수 내에서 내부 함수가 제공된다. 이것은 Inception 영화와 같은 프로그래밍처럼 들릴 수 있지만, 잠시 후에 이 모든 것을 풀어보겠다.

```python
def repeat(num_times):
    def decorator_repeat(func):
        @functools.wraps(func)
        def wrapper_repeat(*args, **kwargs):
            for _ in range(num_times):
                value = func(*args, **kwargs)
            return value
        return wrapper_repeat
    return decorator_repeat
```

약간 지저분해 보이지만, 우리는 decorator에 대한 인수를 처리하는 추가적인 정의 안에 지금까지 여러분이 여러 번 보았던 것과 같은 decorator 패턴을 넣을 뿐이다. 가장 안쪽에 있는 함수부터 시작하겠다.

```python
def wrapper_repeat(*args, **kwargs):
    for _ in range(num_times):
        value = func(*args, **kwargs)
    return value
```

이 `wrapper_repeat()` 함수는 임의의 인수를 사용하고 decorate된 함수 `func()`의 값을 반환한다. 이 wrapper 함수는 또한 decorate 함수를 `num_times`번 호출하는 루프를 포함한다. 이것은 외부에서 공급되어야 하는 `num_times` 매개 변수를 사용한다는 점을 제외하고는 여러분이 본 이전의 wrapper 함수와 다르지 않다.

한 걸음만 나가면 decorate 기능을 찾을 수 있다.

```python
def decorator_repeat(func):
    @functools.wraps(func)
    def wrapper_repeat(*args, **kwargs):
        ...
    return wrapper_repeat
```

다시 말하지만, `decorator_repeat()`는 이름이 다르다는 것을 제외하고는 앞에서 작성한 decorator 함수와 정확히 동일하다. 이는 사용자가 호출할 가장 바깥쪽 함수에 대한 기본 이름 `repeat()`을 사용하기 때문이다.

이미 본 것처럼 가장 바깥쪽 함수는 장식자 함수에 대한 참조를 반환합니다.

```python
def repeat(num_times):
    def decorator_repeat(func):
        ...
    return decorator_repeat
```

`repeat()` 함수에는 다음과 같은 몇 가지 미묘한 문제가 발생한다.

- `decorator_repeat()`을 내부 함수로 정의하는 것은 `repeat()`가 함수 객체인 `decorator_repeat`를 참조할 것이라는 것을 의미한다. 이전에, 우리는 함수 객체를 참조하기 위해 괄호 없이 `repeat`를 사용했다. 추가된 소괄호는 인수를 사용하는 decorator를 정의할 때 필요하다.
- `num_times` 인수는 `repeat()` 자체에서 사용되지 않는 것 같다. 그러나 `num_times`를 통과하면 나중에 `wrapper_repeat()`가 사용할 때까지 `num_times` 값이 저장되는 [closure](https://realpython.com/inner-functions-what-are-they-good-for/)가 생성된다.

모든 설정을 통해 결과가 예상대로 나오는지 확인해 보자.

```python
@repeat(num_times=4)
def greet(name):
    print(f"Hello {name}")
```

```python
>>> greet("World")
Hello World
Hello World
Hello World
Hello World
```

바로 우리가 목표로 했던 결과이다.

### <a name='decorators-optional-arguments'>둘 다 주세요, 하지만 빵은 됐어요</a>
약간의 주의를 기울여 인수와 인수 없이 모두 사용할 수 있는 decorator를 정의할 수도 있다. 대부분 이것이 필요하지 않지만, 유연성이 있어서 좋다.

앞 절에서 본 것처럼 decorator가 인수를 사용할 때는 외부 함수를 추가해야 한다. 문제는 코드가 인수가 포함된 상태에서 decorator를 호출하였는지 여부를 파악하는 것이다.

decorator를 인수 없이 호출하는 경우에만 decorate 함수가 직접 전달되기 때문에 함수는 선택적 인수여야 한다. 즉, decorator 인수는 모두 키워드로 지정해야 한다. 특수 `*` 구문을 사용하여 이를 적용할 수 있다. 즉, [다음 매개 변수는 모두 키워드 전용](https://www.python.org/dev/peps/pep-3102/)임을 의미한다.

```python
def name(_func=None, *, kw1=val1, kw2=val2, ...):  # 1
    def decorator_name(func):
        ...  # Create and return a wrapper function.

    if _func is None:
        return decorator_name                      # 2
    else:
        return decorator_name(_func)               # 3
```

여기서 `_func` 인수는 decorator가 인수로 호출되었는지 여부에 주목하는 마커 역할을 한다.

1. 인수 없이 이름이 호출된 경우, decorate된 함수는 `_func`로 전달된다. 인수와 같이 호출된 경우 `_func`는 없음이 되며 키워드 인수 중 일부가 기본값에서 변경되었을 수 있다. 인수 목록의 `*`는 나머지 인수를 positional 인수로 호출할 수 없음을 의미한다.
2. 이 경우에, decorator는 인수와 함께 호출되었다. 함수를 읽고 반환할 수 있는 dexorator 함수를 반환한다.
3. 이 경우에는 인수 없이 decorator를 호출하였다. decorator를 즉시 함수에 적용한다.

이전 섹션의 `@repeat` decorator에서 이 일상의 코드을 사용하여 다음을 작성할 수 있다.

```python
def repeat(_func=None, *, num_times=2):
    def decorator_repeat(func):
        @functools.wraps(func)
        def wrapper_repeat(*args, **kwargs):
            for _ in range(num_times):
                value = func(*args, **kwargs)
            return value
        return wrapper_repeat

    if _func is None:
        return decorator_repeat
    else:
        return decorator_repeat(_func)
```

원본 `@repeat`와 비교해 보시오. 변경 사항은 추가된 `_func` 매개 변수와 마지막에 `if-else` 뿐이다.

훌륭한 [Python Cookbook](https://realpython.com/asins/1449340377/)의 [레시피 9.6](https://github.com/dabeaz/python-cookbook/blob/master/src/9/defining_a_decorator_that_takes_an_optional_argument/example.py)은 [functools.partial()](https://docs.python.org/library/functools.html#functools.partial)을 사용한 대체 솔루션을 보여준다.

다음 예제는 인수와 함께 또는 인수 없이 `@repeat`을 사용할 수 있음을 보인다.

```python
@repeat
def say_whee():
    print("Whee!")

@repeat(num_times=3)
def greet(name):
    print(f"Hello {name}")
```

num_times의 기본값은 2이다.

```python
>>> say_whee()
Whee!
Whee!

>>> greet("Penny")
Hello Penny
Hello Penny
Hello Penny
```

### <a name='stateful-decorators'>상태를 저장하는 decorator</a>
때때로, **상태를 추적할 수 있는 decorator**가 유용하다. 간단한 예로, 함수가 호출되는 횟수를 세는 decorator를 만들려고 한다.

> **Note:** 이 [튜토리얼의 시작 부분](../primer-decorators/#functions)에서, 우리는 주어진 인수에 기초한 값을 반환하는 순수 함수에 대해 이야기했다. 상태 저장 decorator는 정반대로 반환 값이 주어진 인수뿐만 아니라 현재 상태에 따라 달라진다.

[다음 섹션](#classes-as-decorators)에서는 클래스를 사용하여 상태를 유지하는 방법에 대해 설명한다. 그러나 간단한 경우에는 다음과 같은 기능 속성을 사용해도 무방하다.

```python
import functools

def count_calls(func):
    @functools.wraps(func)
    def wrapper_count_calls(*args, **kwargs):
        wrapper_count_calls.num_calls += 1
        print(f"Call {wrapper_count_calls.num_calls} of {func.__name__!r}")
        return func(*args, **kwargs)
    wrapper_count_calls.num_calls = 0
    return wrapper_count_calls

@count_calls
def say_whee():
    print("Whee!")
```

상태(함수 호출 수)는 wrapper 함수의 함수 속성 `.num_calls`에 저장된다. 다음 경웨 이 기능을 사용하면 효과적이다.

```python
>>> say_whee()
Call 1 of 'say_whee'
Whee!

>>> say_whee()
Call 2 of 'say_whee'
Whee!

>>> say_whee.num_calls
2
```

### <a name='classes-as-decorators'>decorator로 클래스</a>
상태를 유지하는 일반적인 방법은 [클래스를 사용](https://realpython.com/python3-object-oriented-programming/)하는 것이다. 이 섹션에서는 **클래스를 decorator로 사용**하여 이전 섹션의 `@count_calls` 예를 다시 작성하는 방법에 대해 설명한다.

decorator 구문 `@my_decomator`가 `func = my_decomator(func)`를 말하는 더 쉬운 방법일 뿐이라는 것을 기억하세요. 따라서 `my_decorator`가 클래스라면 `func`를 그것의 `__init_()` 메서드 인수로 받아들여야 한다. 게다가, 클래스 인스턴스는 decorate된 함수를 대신할 수 있도록 호출 가능해야 한다.

클래스 인스턴스를 호출 가능하도록 특별한 `.__call__()` 메서드를 구현한다.

```python
class Counter:
    def __init__(self, start=0):
        self.count = start

    def __call__(self):
        self.count += 1
        print(f"Current count is {self.count}")
```

`.__call__()` 메서드는 클래스 인스턴스를 호출할 때마다 실행된다.

```python
import functools

class CountCalls:
    def __init__(self, func):
        functools.update_wrapper(self, func)
        self.func = func
        self.num_calls = 0

    def __call__(self, *args, **kwargs):
        self.num_calls += 1
        print(f"Call {self.num_calls} of {self.func.__name__!r}")
        return self.func(*args, **kwargs)

@CountCalls
def say_whee():
    print("Whee!")
```

`.__init__()` 메서드는 함수에 대한 참조를 저장해야 하며 다른 필요한 초기화를 수행할 수 있다. decorate된 함수 대신 `.__call__()` 메서드가 호출된다. 이는 기본적으로 우리의 초기 예에서 `wrapper()` 함수과 같은 일을 한다. `@functools.wraps` 대신 `functools.update_wrapper()` 함수를 사용해야 한다.

이 `@CountCalls` decorator는 이전 섹션의 decorator와 동일하게 작동합니다.

```python
>>> say_whee()
Call 1 of 'say_whee'
Whee!

>>> say_whee()
Call 2 of 'say_whee'
Whee!

>>> say_whee.num_calls
2
```

## <a name='more-real-world-examples'>더 많은 실전 예</a>
우리는 모든 종류의 장식가decorator들을 만드는 방법을 배웠기 때문에, 이제 먼 길을 왔다. 마지막으로, 우리의 새로운 지식을 실제 세계에서 실제로 유용할 수 있는 몇 가지 사례를 더 만드는 것으로 마무리하고자 한다.

### <a name='slowing-down-code'>감속 코드, 재검토</a>
앞서 언급했듯이, `@slow_down`의 이전 구현은 항상 1초 동안 절전 모드로 전환된다. 이제 decorator에 매개 변수를 추가하는 방법을 알았으므로 절전 모드의 시간을 제어하는 선택적 속도 인수를 사용하여 `@slow_down`을 다시 작성해 보겠다.

```python
import functools
import time

def slow_down(_func=None, *, rate=1):
    """Sleep given amount of seconds before calling the function"""
    def decorator_slow_down(func):
        @functools.wraps(func)
        def wrapper_slow_down(*args, **kwargs):
            time.sleep(rate)
            return func(*args, **kwargs)
        return wrapper_slow_down

    if _func is None:
        return decorator_slow_down
    else:
        return decorator_slow_down(_func)
```

우리는 [둘 다 주세요, 하지만 빵은 됐어요](#decorators-optional-arguments) 섹션에 소개된 상용어를 사용하여 인수가 있거나 없는 `@slow_down`을 호출할 수 있도록 하고 있다. [이전](primer-decorators.md#slowing-down-code)과 동일한 재귀 `countdown()` 함수는 이제 각 카운트 간에 2초간 절전 모드로 전환된다.

```python
@slow_down(rate=2)
def countdown(from_number):
    if from_number < 1:
        print("Liftoff!")
    else:
        print(from_number)
        countdown(from_number - 1)
```

이전과 마찬가지로 decorator의 효과를 보려면 사용자가 직접 예제를 실행해야 한다.

```python
>>> countdown(3)
3
2
1
Liftoff!
```

### <a name='creating-singletins'>싱글톤 만들기</a>
싱글턴은 인스턴스가 하나뿐인 클래스이다. Python에는 `None`, `True` 및 `False`를 포함하여 자주 사용하는 여러 싱글톤이 있다. `None`은 싱글턴으로 [둘 다 주세요, 하지만 빵은 됐어요](#decorators-optional-arguments) 섹션에서 볼 수 있었듯이 `is kwyword`를 사용하여 `None`을 비교할 수 있다. 

```python
if _func is None:
    return decorator_name
else:
    return decorator_name(_func)
```

`is`를 사용하면 정확히 동일한 인스턴스인 개체에 대해서만 `True`를 반환한다. 다음 `@singleton` decorator는 클래스의 첫 번째 인스턴스를 속성으로 저장하여 클래스를 싱글톤으로 바꾼다. 나중에 인스턴스를 생성하려고 하면 저장된 인스턴스를 반환한다.

```python
import functools

def singleton(cls):
    """Make a class a Singleton class (only one instance)"""
    @functools.wraps(cls)
    def wrapper_singleton(*args, **kwargs):
        if not wrapper_singleton.instance:
            wrapper_singleton.instance = cls(*args, **kwargs)
        return wrapper_singleton.instance
    wrapper_singleton.instance = None
    return wrapper_singleton

@singleton
class TheOne:
    pass
```

보다시피 이 클래스 decorator는 함수 decorator와 동일한 템플릿을 따른다. 유일한 차이점은 우리는 그것이 클래스 decorator를 의미한다는 것을 나타내기 위해 func 대신 cls를 매개 변수 이름으로 사용하고 있다는 것이다.

효과가 있는지 확인해 보자.

```python
>>> first_one = TheOne()
>>> another_one = TheOne()

>>> id(first_one)
140094218762280

>>> id(another_one)
140094218762280

>>> first_one is another_one
True
```

`first_one()`이 `another_one()`과 정확히 동일하다는 것이 분명해 보인다.

> **Note:** 싱글턴 클래스는 Pythn에서 다른 언어만큼 자주 사용되지 않는다. 싱글톤의 효과는 일반적으로 모듈에서 전역 변수로 더 잘 사용된다.

### <a name='caching-return-values'>캐싱 반환 값</a>
decorator는 [캐싱](https://en.wikipedia.org/wiki/Cache_%28computing%29)과 [메모리화(memorization)](https://en.wikipedia.org/wiki/Memoization)를 위한 멋진 메커니즘을 제공할 수 있다. 예를 들어 [피보나치 수열](https://en.wikipedia.org/wiki/Fibonacci_number)의 [재귀적](https://realpython.com/python-thinking-recursively/) 정의를 살펴보자.

```python
from decorators import count_calls

@count_calls
def fibonacci(num):
    if num < 2:
        return num
    return fibonacci(num - 1) + fibonacci(num - 2)
```

구현은 간단하지만 런타임 성능은 형편없다.

```python
>>> fibonacci(10)
<Lots of output from count_calls>
55

>>> fibonacci.num_calls
177
```

10번째 피보나치 수를 계산하기 위해서는 앞의 피보나치 수만 계산하면 되지만, 이 구현체는 어떻게든 엄청난 177개의 계산이 필요하다. 그것은 빠르게 악화된다. `fibonacci(20)`를 위해서는 21891개의 계산이 필요하고 30번째 숫자를 위해서는 거의 270만 개의 계산이 필요하다. 코드가 이미 알려진 피보나찌 숫자를 계속 재계산하기 때문이다.

일반적인 해결책은 [for 루프](https://realpython.com/python-for-loop/)와 룩업 테이블을 사용하여 피보나치 수를 구현하는 것이다. 그러나 계산의 단순 캐싱도 다음과 같은 이점을 제공한다.

```python
import functools
from decorators import count_calls

def cache(func):
    """Keep a cache of previous function calls"""
    @functools.wraps(func)
    def wrapper_cache(*args, **kwargs):
        cache_key = args + tuple(kwargs.items())
        if cache_key not in wrapper_cache.cache:
            wrapper_cache.cache[cache_key] = func(*args, **kwargs)
        return wrapper_cache.cache[cache_key]
    wrapper_cache.cache = dict()
    return wrapper_cache

@cache
@count_calls
def fibonacci(num):
    if num < 2:
        return num
    return fibonacci(num - 1) + fibonacci(num - 2)
```

캐시는 룩업 테이블로 작동하므로 `fibonacci()`는 필요한 계산을 한 번만 수행한다.

```python
>>> fibonacci(10)
Call 1 of 'fibonacci'
...
Call 11 of 'fibonacci'
55

>>> fibonacci(8)
21
```

`fibonacci(8)`에 대한 마지막 호출에서 8번째 피보나치 수는 이미 피보나치(10) 호출에서 계산되었기 때문에 새로운 계산이 필요하지 않았다.

표준 라이브러리에서 `@functools.lru_cache`로 [LRU(Lest Recently Used) 캐시](https://realpython.com/lru-cache-python/)를 사용할 수 있다. 

이 decorator는 위에서 본 것보다 더 많은 기능을 가지고 있다. 캐시 decorator를 직접 작성하는 대신 `@functools.lru_cache`를 사용해야 한다.

```python
import functools

@functools.lru_cache(maxsize=4)
def fibonacci(num):
    print(f"Calculating fibonacci({num})")
    if num < 2:
        return num
    return fibonacci(num - 1) + fibonacci(num - 2)
```

`maxsize` 매개 변수는 캐시된 최근 호출 수를 지정한다. 기본값은 128이지만 모든 함수 호출을 캐시하기 위하여 `maxsize=None`를 지정할 수 있다. 그러나 큰 개체를 많이 캐시하는 경우 이로 인해 메모리 문제가 발생할 수 있다.

`.cache_info()` 메서드를 사용하여 캐시의 성능을 확인하고 필요한 경우 조정할 수 있다. 이 예에서는 캐시에서 요소가 제거되는 효과를 확인하기 위해 인위적으로 작은 `maxsize`를 사용했다.

```python
>>> fibonacci(10)
Calculating fibonacci(10)
Calculating fibonacci(9)
Calculating fibonacci(8)
Calculating fibonacci(7)
Calculating fibonacci(6)
Calculating fibonacci(5)
Calculating fibonacci(4)
Calculating fibonacci(3)
Calculating fibonacci(2)
Calculating fibonacci(1)
Calculating fibonacci(0)
55

>>> fibonacci(8)
21

>>> fibonacci(5)
Calculating fibonacci(5)
Calculating fibonacci(4)
Calculating fibonacci(3)
Calculating fibonacci(2)
Calculating fibonacci(1)
Calculating fibonacci(0)
5

>>> fibonacci(8)
Calculating fibonacci(8)
Calculating fibonacci(7)
Calculating fibonacci(6)
21

>>> fibonacci(5)
5

>>> fibonacci.cache_info()
CacheInfo(hits=17, misses=20, maxsize=4, currsize=4)
```

### <a name='adding-information-about-units'>단위에 대한 정보 추가</a>
다음 예제는 decorate된 함수의 동작을 실제로 변경하지 않는다는 점에서 이전의 [등록 플러그인]() 예제와 다소 유사하다. 대신 간단히 함수 속성으로 `unit`를 추가한다.

```python
def set_unit(unit):
    """Register a unit on a function"""
    def decorator_set_unit(func):
        func.unit = unit
        return func
    return decorator_set_unit
```

다음 예제에서는 센티미터 단위로 반지름과 높이를 기준으로 실린더의 부피를 계산한다.

```python
import math

@set_unit("cm^3")
def volume(radius, height):
    return math.pi * radius**2 * height
```

이 `.unit` 함수 속성은 나중에 필요할 때 액세스할 수 있다.

```python
>>> volume(3, 5)
141.3716694115407

>>> volume.unit
'cm^3'
```

함수 주석(annotation)을 사용하여 유사한 결과를 얻을 수 있었다.

```python
import math

def volume(radius, height) -> "cm^3":
    return math.pi * radius**2 * height
```

그러나 주석은 [타입 힌트에 사용](https://www.python.org/dev/peps/pep-0484/)되므로 주석과 같은 단위를 [정적 타입 검사](https://realpython.com/python-type-checking/#static-type-checking)와 결합하기는 어려울 것이다.

유닛 간 변환이 가능한 라이브러리와 연결되면 유닛이 더욱 강력해지고 재미있어 진다. 그러한 라이브러리 중 하나가 [pint](http://pint.readthedocs.io/)이다. 예를 들어 `pint`가 설치된 경우([pip install Pint](https://pypi.org/project/Pint/)) 부피를 입방 인치 또는 갤런으로 변환할 수 있다.

```python
>>> import pint
>>> ureg = pint.UnitRegistry()
>>> vol = volume(3, 5) * ureg(volume.unit)

>>> vol
<Quantity(141.3716694115407, 'centimeter ** 3')>

>>> vol.to("cubic inches")
<Quantity(8.627028576414954, 'inch ** 3')>

>>> vol.to("gallons").m  # Magnitude
0.0373464440537444
```

decorator를 수정하여 직접 `pint` [Quantity](https://pint.readthedocs.io/en/latest/tutorial.html)를 반환할 수도 있다. 이러한 `Quantity`는 단위에 값을 곱하여 계산된다. `pint`에서 `UnitRegistry`에서`units`를 조회해야 한다. 레지스트리는 네임스페이스가 복잡해지는 것을 방지하기 위해 함수 속성으로 저장한다.

```python
def use_unit(unit):
    """Have a function return a Quantity with given unit"""
    use_unit.ureg = pint.UnitRegistry()
    def decorator_use_unit(func):
        @functools.wraps(func)
        def wrapper_use_unit(*args, **kwargs):
            value = func(*args, **kwargs)
            return value * use_unit.ureg(unit)
        return wrapper_use_unit
    return decorator_use_unit

@use_unit("meters per second")
def average_speed(distance, duration):
    return distance / duration
```

`@use_unit` decorator를 사용하면 단위 변환이 실질적으로 용이하다.

```python
>>> bolt = average_speed(100, 9.58)
>>> bolt
<Quantity(10.438413361169102, 'meter / second')>

>>> bolt.to("km per hour")
<Quantity(37.578288100208766, 'kilometer / hour')>

>>> bolt.to("mph").m  # Magnitude
23.350065679064745
```

### <a name='validating-json'>JSON 유효성 검사</a>
마지막 사용 사례를 살펴보겠다. 다음 [Flask](https://realpython.com/tutorials/flask/) 경로 핸들러를 간단히 살펴본다.

```python
@app.route("/grade", methods=["POST"])
def update_grade():
    json_data = request.get_json()
    if "student_id" not in json_data:
        abort(400)
    # Update database
    return "success!"
```

여기서는 키 `student_id`가 요청의 일부임을 확인한다. 이 유효성 검사는 작동하지만, 실제로는 기능 자체에 속하지 않는다. 또한  다른 경로에서도 동일한 유효성 검사를 사용할 수 있다. 그러니, [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) 상태를 유지하고 불필요한 논리를 decorator로 추상화해 보자. 다음 `@validate_json` decorator가 이 작업을 수행한다.

```python
from flask import Flask, request, abort
import functools
app = Flask(__name__)

def validate_json(*expected_args):                  # 1
    def decorator_validate_json(func):
        @functools.wraps(func)
        def wrapper_validate_json(*args, **kwargs):
            json_object = request.get_json()
            for expected_arg in expected_args:      # 2
                if expected_arg not in json_object:
                    abort(400)
            return func(*args, **kwargs)
        return wrapper_validate_json
    return decorator_validate_json
```

위의 코드에서 decorator는 변수 길이 목록을 인수로 사용하여 필요한 만큼의 문자열 인수를 전달할 수 있으며, 각각은 [JSON](https://realpython.com/python-json/) 데이터를 검증하는 데 사용되는 키를 나타낸다:

1. JSON에 존재해야 하는 키 목록은 decorator에 대한 인수로 제공된다.
2. wrapper 함수는 예상되는 각 키가 JSON 데이터에 있는지 확인한다.

그러면 루트 핸들러는 JSON 데이터가 안전하게 유효하다고 가정할 수 있기 때문에 실제 작업인 등급 업데이트에 집중할 수 있다.

```python
@app.route("/grade", methods=["POST"])
@validate_json("student_id")
def update_grade():
    json_data = request.get_json()
    # Update database.
    return "success!"
```

## <a name='conclusion'>결론</a>
꽤 긴 여정이었어요! 이 튜토리얼은 함수에 대해 조금 더 자세히 살펴 보는 것으로 시작되었고, 특히 다른 함수 내부에서 함수를 정의되고 다른 Python 객체와 마찬가지로 함수를 전달될 수 있는 방법에 대해 조금 더 자세히 살펴 보았다. 그후 여러분은 decorator와 그것들을 사용하는 방법에 대해 배웠다.

- decorator를 재사용될 수 있다.
- 인수와 반환 값으로 함수를 decorate할 수 있다.
- decorator는 decorate된 함수와 더 비슷하게 보이도록 `@funtools.wraps`를 사용할 수 있다.

튜토리얼의 두 번째 부분인 고급 decorator에서 다음과 같은 방법을 공부하였다.

- 클래스 decorate
- 중첩 decorators
- decorator에 인수 추가
- decortaor에서 상태 유지
- decorator로 클래스 사용

decortaor를 정의하기 위해 일반적으로 wrapper 함수를 반환하는 함수를 정의한다. wrapper 함수는 `*args`와 `**kwrgs`를 사용하여 인수를 decorate된 함수에 전달한다. decorator가 인수를 사용하도록 하려면 wrapper 함수를 다른 함수 안에 중첩해야 한다. 이 경우 일반적으로 세 개의 반환문이 표시된다.

[이 튜토리얼에서 온라인으로 코드](https://github.com/realpython/materials/tree/master/primer-on-python-decorators)를 찾을 수 있습니다.

**That's it, 수고하셨습니다!**
