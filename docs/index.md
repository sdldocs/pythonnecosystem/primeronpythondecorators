
decorator에 대한 이 튜토리얼에서는 decorator가 무엇인지, decorator를 만들고 사용하는 방법에 대해 알아본다. decorator는 [고차 함수(high-order function)](http://en.wikipedia.org/wiki/Higher-order_function)를 호출하기 위한 간단한 구문을 제공한다.

정의에 따르면, decorator는 한 함수에 다른 함수을 취하여 전자의 함수를 명시적으로 수정하지 않고 그 함수의 동작을 확장하는 함수이다.

이것은 혼란스럽게 들리지만, 특히 decorator들이 어떻게 작동하는지에 대한 몇 가지 예를 본 후에는 혼란스럽지 않을 것이다. 여러분은 [여기서](https://github.com/realpython/materials/tree/master/primer-on-python-decorators) 이 튜토리얼의 모든 예를 찾을 수 있다.

## 차례

- [Functions](primer-decorators.md#functions)
    - [First-Class 객체](primer-decorators.md#first-class-objects)
    - [내부 함수](primer-decorators.md#inner-functions)
    - [함수에서 함수 반환](primer-decorators.md#returning-functions-from-functions)

- [간단한 Decorators](primer-decorators.md#simple-decorators)
    - [Syntactic Sugar!](primer-decorators.md#syntactic-sugar)
    - [Decorators 재활용](primer-decorators.md#reusing-decorators)
    - [인수가 있는 Decorating 함수](primer-decorators.md#decorating-with-arguments)
    - [Decorated 함수의 반환 값](primer-decorators.md#returning-values)
    - [Who Are You, Really?](primer-decorators.md#who-are-you)

- [실전 예](primer-decorators.md#real-world-examples)
    - [타이밍 함수](primer-decorators.md#timing-functions)
    - [디버깅 코드](primer-decorators.md#debugging-code)
    - [감속 코드](primer-decorators.md#slowing-down-code)
    - [플러그인 등록](primer-decorators.md#reginstering-plugin)
    - [로그인한 사용자?](primer-decorators.md#user-logged-in)

- [멋진 decorator](advanced.md#fancy-decorators)
    - [틀래스 decorating](advanced.md#decorating-classes)
    - [중첩 decorator](advanced.md#nesting-decorators)
    - [인수가 있는 decorator](advanced.md#decorators-with-arguments)
    - [둘 다 주세요, 하지만 빵은 됐어요](advanced.md#decorators-optional-arguments)
    - [상태를 저장하는 decorators](advanced.md#stateful-decorators)
    - [decorator로 클래스](advanced.md#classes-as-decorators)   

- [더 많은 실전 예](advanced.md#more-real-world-examples)
    - [감속 코드, 재검토](advanced.md#slowing-down-code)
    - [싱글톤 만들기](advanced.md#creating-singletins)
    - [캐싱 반환 값](advanced.md#caching-return-values)
    - [단위에 대한 정보 추가](advanced.md#adding-information-about-units)
    - [JSON 유효성 검사](advanced.md#validating-json)

- [결론](advanced.md#conclusion)

